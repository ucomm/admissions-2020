<article id="content-none">
  <div class="container title-container">
    <h1><?php esc_html_e('No results', 'admissions-2020'); ?></h1>
  </div>
  <div class="container">
    <p>
      <a href="<?php echo home_url(); ?>">
        <?php esc_html_e('Return to the homepage', 'admissions-2020'); ?>
      </a>
    </p>
  </div>
</article>
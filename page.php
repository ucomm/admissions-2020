<?php

use Admissions2020\Lib\BeaverBuilder\Helpers as BBHelpers;

$is_bb_enabled = BBHelpers::isBuilderEnabled();

$layout = $is_bb_enabled ? 'default.php' : 'sidebar.php';

include(ADMISSIONS_2020_DIR . '/layouts/' . $layout);

#!/bin/bash

# zip the project with composer
# this uses composers exclude property to ignore files
composer archive --format=zip --file "./$FILENAME"

# bitbucket credentials are in jenkins and exposed as global variables
curl --location --request POST --user "${BITBUCKET_API_USERNAME}:${BITBUCKET_API_PASSWORD}" "https://api.bitbucket.org/2.0/repositories/ucomm/${BITBUCKET_SLUG}/downloads" \
--form files=@"./$FILENAME.zip"
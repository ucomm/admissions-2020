#!/bin/bash

# install composer dependencies
COMPOSER_DISCARD_CHANGES=1 composer install --no-dev --no-scripts --no-interaction || exit 1
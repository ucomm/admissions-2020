<?php

/**
 * 
 * Create a social icon link based on the current widget instance
 * 
 */

  if (is_array($instance)) {
    $title = isset($instance['a11y-title']) ? $instance['a11y-title'] : '';
    $icon = isset($instance['social-icon']) ? $instance['social-icon'] : '';
    $link = isset($instance['social-link']) ? $instance['social-link'] : '';
  ?>
    <a href="<?php echo $link; ?>" class="social-link footer-social-link" title="<?php echo $title; ?>">
      <?php echo $this->getSVG($icon); ?>
    </a>
  <?php
    }
  ?>
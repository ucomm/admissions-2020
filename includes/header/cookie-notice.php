    <script src="https://ucommobjectstorage.blob.core.windows.net/cookie-jar/cookie-notification.js" type="text/javascript"></script>
    <noscript>
      <p>Our websites may use cookies to personalize and enhance your experience. By continuing without changing your cookie settings, you agree to this collection. For more information, please see our <a href="https://privacy.uconn.edu/university-website-notice/" target="_blank">University Websites Privacy Notice</a>.</p>
    </noscript>
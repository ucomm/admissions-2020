<div class="container pagination-container">
  <?php
  the_posts_pagination([
    'screen_reader_text' => ''
  ]);
  ?>
</div>
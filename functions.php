<?php

define('ADMISSIONS_2020_DIR', get_stylesheet_directory());
define('ADMISSIONS_2020_URL', get_stylesheet_directory_uri());

if (!defined('OBJECT_STORAGE_URL')) {
  define('OBJECT_STORAGE_URL', 'https://ucommobjectstorage.blob.core.windows.net/uconn-cdn-files');
}

if (!defined('OBJECT_STORAGE_FONTAWESOME_URL')) {
  define('OBJECT_STORAGE_FONTAWESOME_URL', OBJECT_STORAGE_URL . '/shared/icons/legacy-font-awesome-6.4.2');
}

require_once(ABSPATH . 'wp-admin/includes/plugin.php');
require_once('lib/AbstractAssetLoader.php');
// require_once('lib/Admin/MenuPage.php');
// require_once('lib/Admin/Options.php');
require_once('lib/Admin/Customizer.php');
require_once('lib/Admin/ThemeSupports.php');
require_once('lib/BeaverBuilder/FormHelpers.php');
require_once('lib/BeaverBuilder/Helpers.php');
require_once('lib/BeaverBuilder/StyleHelpers.php');
require_once('lib/ContentManager.php');
require_once('lib/Footer.php');
require_once('lib/Header.php');
require_once('lib/Menus.php');
require_once('lib/ScriptLoader.php');
require_once('lib/StyleLoader.php');
require_once('lib/Widgets/SocialWidget.php');
require_once('lib/Widgets/Widgets.php');

if (file_exists(dirname(ABSPATH) . '/vendor/autoload.php')) {
  require_once(dirname(ABSPATH) . '/vendor/autoload.php');
} elseif (file_exists(ABSPATH . 'vendor/autoload.php')) {
  require_once(ABSPATH . 'vendor/autoload.php');
} else {
  require_once('vendor/autoload.php');
}

use Admissions2020\Lib\Admin\ThemeSupports;
use Admissions2020\Lib\BeaverBuilder\Helpers as BBHelpers;
use Admissions2020\Lib\Menus;
use Admissions2020\Lib\ScriptLoader;
use Admissions2020\Lib\StyleLoader;
// use Admissions2020\Lib\Admin\MenuPage;
use Admissions2020\Lib\Widgets\Widgets;
use Admissions2020\Lib\Admin\Customizer;

$scriptLoader = new ScriptLoader();
$scriptLoader->enqueueAssets();

$styleLoader = new StyleLoader();
$styleLoader->enqueueAssets();

$customizer = new Customizer();
$customizer->customizeRegister();

if (is_admin()) {
  // add settings page if necessary
  // $menuPage = new MenuPage();
  // $menuPage->createMenuPage();

  $scriptLoader->enqueueAdminAssets();
  $styleLoader->enqueueAdminAssets();
}

$menus = new Menus();
$menus->handleMenusAfterSetup();

$widgets = new Widgets();
$widgets->widgetsInit();

$themeSupports = new ThemeSupports();
$themeSupports->afterSetupTheme();
$themeSupports->initPostTypeSupport();

if (class_exists('FLBuilder')) {
  $bb_helpers = new BBHelpers();
  $bb_helpers->addCustomFonts();
  $bb_helpers->filterSettingsForms();
  $bb_helpers->filterModuleCSS();
  add_filter('fl_user_has_unfiltered_html', '__return_true');
}

@ini_set( 'upload_max_size' , '98M' );
@ini_set( 'post_max_size', '98M');
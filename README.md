# Admissions 2020 Theme
This theme replicates the current (2021) admissions site design in WordPress instead of Drupal.

## Usage
- `nvm use`
- `npm install` (first time only)
- `npm run dev`
- in a new shell `docker-compose up`


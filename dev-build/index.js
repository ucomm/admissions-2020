/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/sass/main.scss":
/*!****************************!*\
  !*** ./src/sass/main.scss ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!*************************!*\
  !*** ./src/js/index.js ***!
  \*************************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _sass_main_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../sass/main.scss */ "./src/sass/main.scss");

// enable the javascript for the main nav area
const navMenuIDs = ['am-main-menu', 'am-mobile-nav'];
navMenuIDs.forEach(menuId => {
  const navigation = new Navigation({
    menuId,
    click: true
  });
  navigation.init();
});

// handle mobile nav trigger events
const mobileTrigger = document.querySelector('#mobile-trigger');
const mobileNavContainer = document.querySelector('#mobile-nav-container');
document.addEventListener('keydown', evt => {
  const mobileNavOpen = mobileNavContainer.classList.contains('open');
  if (evt.key !== 'Escape' || !mobileNavOpen) return;
  if (mobileNavOpen) {
    mobileTrigger.innerText = 'MENU';
    mobileTrigger.setAttribute('aria-expanded', 'false');
    mobileNavContainer.classList.remove('open');
  }
});
mobileTrigger.addEventListener('click', () => {
  mobileTrigger.innerText = mobileTrigger.innerText === 'MENU' ? 'CLOSE' : 'MENU';
  mobileTrigger.getAttribute('aria-expanded') === 'true' ? mobileTrigger.setAttribute('aria-expanded', 'false') : mobileTrigger.setAttribute('aria-expanded', 'true');
  mobileNavContainer.classList.toggle('open');
});
document.addEventListener('DOMContentLoaded', () => {
  const outboundLinks = document.querySelectorAll("#main-nav-menu > #am-main-menu > li > ul > .am-outbound > a");
  const outboundLinksMobile = document.querySelectorAll("#mobile-nav-menu > #am-mobile-nav > li > ul > .am-outbound > a");
  for (var i = 0; i < outboundLinks.length; i++) {
    outboundLinks[i].setAttribute('target', '_blank');
  }
  for (var j = 0; j < outboundLinksMobile.length; j++) {
    outboundLinksMobile[j].setAttribute('target', '_blank');
  }
});
})();

/******/ })()
;
//# sourceMappingURL=index.js.map
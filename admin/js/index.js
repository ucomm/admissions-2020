const socialWidgets = document.querySelectorAll('div[id*="uconn-social-widget"].widget')
const socialWidgetSelects = document.querySelectorAll('.social-widget-select')

/**
 * 
 * Update the icon preview areas when the page loads or a new icon is chosen.
 * 
 * @param {DOMElement} selectEl - the select element
 */
const handleIcon = selectEl => {
  const iconContainerID = selectEl.getAttribute('data-icon-preview-id')
  const iconContainer = document.querySelector(`#${iconContainerID}`)
  const path = 'content/themes/admissions-2020/images/icons'

  iconContainer.style.backgroundImage = ''
  iconContainer.style.backgroundImage = `url(/${path}/${selectEl.value}.svg)`
}

jQuery(document).on('ready widget-updated change', evt => {
  
  if (evt.type === 'change' && !evt.target.classList.contains('social-widget-select')) {
    return
  }

  socialWidgetSelects.forEach(select => {
    handleIcon(select)
  })

  // reload the page after the widget updates for now.
  // I must be missing something about how wordpress widget areas work.
  // currently, after saving/updating the icon, you can't select a new one without reloading
  if (evt.type === 'widget-updated') {
    window.location.reload()
  }
})
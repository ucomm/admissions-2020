# 1.1.13
- Added support for defined Azure storage urls
- Fixed issue with Beaver Builder preventing users from changing pictures
# 1.1.12
- Updated object storage urls from stackpath (rip) to azure
# 1.1.11
- removed meta description from header
# 1.1.10
- included method to display icons from font-awesome classes to update icons in footer
# 1.1.9
- updated banner to link to uconn main site
- removed draft- prefix from admissions prod url in jenkinsfile
# 1.1.8
- updated name of dry run variable in JenkinsFile, for clarity
# 1.1.7
- updates to main menu to allow for outbound links to open in new tab
# 1.1.6
- prod configuration for jenkinsfile
# 1.1.5
- included banner & styles via vendor directory
- inclusion of Jenkinsfile & related files/scripts
- changed paths of content directory
- all of the above is related to moving admissions off of aurora onto staging0/comm0
# 1.1.4
- updated paths for fonts and cookie notice to reflect new CDN
# 1.1.3
- updated logic surrounding "learn more" link on search result pages
# 1.1.2
- Tweaked styles so that all 5 social media icons stay on one line in all screen sizes
# 1.1.1
- Added support for tiktok social media icon in footer

# 1.1.0
- Added customizer support for "parent" site text and link

# 1.0.7
- Fix for issue with composer archive order

# 1.0.6
- Potential fix for WP 5.8.1 core update that broke the ability for admins to add some raw HTML tags (e.g. svg, iframe, etc)

# 1.0.5
- Fixed banner icon styles so they don't disappear
- General style updates by request
- Updated local dev process
  - .nvmrc file
  - removed local docker image 
  - added readme

# 1.0.4
- Fixed banner site search form action
- Updated styles by request
- Composer dev updates

# 1.0.3
- Update to a11y-menu nav script

# 1.0.2
- Fix to banner search for to direct searches to the correct site
- Fix to mobile menu for nested menus
- Updated styles for slate form integration

# 1.0.1
- Changed theme name and associated variables by request
- Updates to banner for improved site search

# 1.0.0
- Initial release
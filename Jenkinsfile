pipeline {
  agent any
  environment {
    THEME_SLUG = "admissions-2020"
    BITBUCKET_SLUG = "admissions-2020"
    DEV_BRANCH = "develop"
    PROD_BRANCH = "main"
  }
  // handle rsync dry runs
  parameters {
    booleanParam(
      name: 'ADMISSIONS_RSYNC_DRY_RUN',
      defaultValue: true,
      description: 'Toggles the rsync --dry-run flag for the admissions website'
    )
  }
  stages {
    // checkout from git and skip if the "ci skip" message is present in the commit
    stage('Checkout') {
      steps {
        // do not delete builds. there's a breaking issue there
        // https://issues.jenkins.io/browse/JENKINS-66843
        scmSkip(deleteBuild: false)
        // relies on the global notification library
        // https://bitbucket.org/ucomm/jenkins-send-notifications/src/main/
        sendNotifications 'STARTED'
      }
    }
    stage("Prepare Build Assets") {
      // feature branches can be added and/or this "when" block can be removed to build on all commitss
      when {
        anyOf {
          branch "${DEV_BRANCH}";
          branch "${PROD_BRANCH}";
          buildingTag()
        }
      }
      // run these tasks at the same time
      parallel {
        stage('NPM') {
          steps {
            sh "${WORKSPACE}/ci-scripts/npm.sh"
          }
        }
        stage('Composer') {
          steps {
            sh "${WORKSPACE}/ci-scripts/composer.sh"
          }
        }
      }
      post {
        always {
          echo "======== $BRANCH_NAME end asset builds ========"
        }
        success {
          echo "======== success - $BRANCH_NAME asset builds ========"
        }
        failure {
          echo "======== failed - $BRANCH_NAME asset builds ========"
        }
      }
    }
    stage('Dev Pushes') {
      // set environment variables 
      environment {
        FILENAME = "$GIT_BRANCH"
        SITE_DIRECTORY = "edu.uconn.admissions.dev"
      }
      when {
        branch "${DEV_BRANCH}"
      }
      parallel {
        stage('Push to staging0') {
          steps {
            sh "${WORKSPACE}/ci-scripts/dev-push.sh"
          }
        }
        stage('Archive dev to bitbucket') {
          steps {
            sh "${WORKSPACE}/ci-scripts/bitbucket-archive.sh"
          }
        }
      }
    }
    stage('Staging Push') {
      environment {
        SITE_DIRECTORY = "edu.uconn.staging.admissions"
      }
      when {
        branch "${PROD_BRANCH}"
      }
      steps {
        sh "${WORKSPACE}/ci-scripts/dev-push.sh"
      }
      post {
        failure {
          sendNotifications("FAILURE - Push to staging")
        }     
      }
    }
    stage('Prod Push') {
      environment {
        SITE_DIRECTORY = "edu.uconn.admissions"
        FILE_NAME = "${TAG_NAME}"
      }
      when {
        buildingTag()
      }
      parallel {
        stage ('Push to prod') {
          steps {
            sh "${WORKSPACE}/ci-scripts/prod-push.sh"
          }
        }
        stage ('Archive prod to bitbucket') {
          steps {
            script {
              if (env.ADMISSIONS_RSYNC_DRY_RUN == "false") {
                sh "${WORKSPACE}/ci-scripts/bitbucket-archive.sh"
              }
            }
          }
        }
      }
      post {
        failure {
          sendNotifications("FAILURE - Push to prod")
        }
        success {
          // NB - don't use script blocks very much.
          // I'm just adding this so th comm0-updates channel doesn't get flooded on dry runs
          script {
            if (env.ADMISSIONS_RSYNC_DRY_RUN == "false") {
              sendNotifications("SUCCESS", "#comm0-updates", [
                [
                  type: "section",
                  text: [
                    type: "mrkdwn",
                    text: ":tada: *$THEME_SLUG* updated"
                  ]              
                ]
              ])
              slackUploadFile(channel: "#comm0-updates", filePath: "changelog.md", initialComment:  "${THEME_SLUG} Changelog")
            }
          }
        }
      }
    }
  }
  post {
    // send slack notifications when the project finishes
    success {
      sendNotifications 'SUCCESSFUL'
    }
    failure {
      sendNotifications 'FAILED'
    }
    aborted {
      sendNotifications 'ABORTED'
    }
    always {
      echo "======== Cleanup ========"
      sh "rm -rf node_modules"
    }
  }
}
const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin')

const env = process.env.NODE_ENV
const mode = env === 'production' ? 'production' : 'development'
const buildDir = env === 'production' ? 'build' : 'dev-build'

const optimization = {}

if (env === 'production') {
  optimization.minimize = true
  optimization.minimizer = [
    new CssMinimizerPlugin(),
    '...'
  ]
}

module.exports = {
  entry: {
    index: path.resolve(__dirname, 'src', 'js', 'index.js'),
    // main: path.resolve(__dirname, 'src', 'sass', 'main.scss')
  },
  output: {
    path: path.resolve(__dirname, buildDir),
    // filename: '[name].js',
    chunkFilename: '[name][ext]',
  },
  mode,
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        }
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
              importLoaders: 2,
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
              sassOptions: {
                includePaths: ['./node_modules/scss/**/*.scss']
              }
            }
          },
        ]
      },
      {
        // handles images from css
        test: /\.(png|jpg|gif|svg)$/,
        type: 'asset/resource',
        generator: {
          filename: 'images/[name][ext]'
        }
      }
    ]
  },
  resolve: {
    extensions: ['.css', '.scss', '.js', '.json']
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css'
    })
  ],
}
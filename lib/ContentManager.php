<?php

namespace Admissions2020\Lib;

use Admissions2020\Lib\Menus;

class ContentManager {
  public function loop(string $slug = 'template-parts/content', string $name = 'loop', array $args = []) {
    if (have_posts()) {
      while (have_posts()) {
        the_post();
        get_template_part($slug, $name, $args);
      }
    } else {
      get_template_part($slug, 'none');
    }
  }

  public function excerptMoreLink() {
    add_filter('excerpt_more','__return_false'); // except_more is only called for non-manually added excerpts. These excerpts have an annoying ellipsis, so let's lose that. 
    add_filter( 'get_the_excerpt', [$this, 'prepareMoreLink'] ); //NOW use get_the_excerpt hook to get all excerpts (manually-added or not) and throw back a learn more link
  }

  public function prepareMoreLink(string $more): string {

    global $post;

    return $more .= ' <a class="moretag" href="' . get_permalink($post->ID) . '" aria-label="Read more about ' . get_the_title($post->ID) . '">Learn more...</a>';
  }

  public function displayPaginationLinks() {
    ob_start();
    include(ADMISSIONS_2020_DIR . '/includes/post-pagination.php');
    echo ob_get_clean();
  }

  public function getFooter() {
    get_footer();
  }

  public function getHeader() {
    get_header();
  }

  public function createSidebarMenu() {
    return (new Menus)->createSidebarMenu();
  }
}
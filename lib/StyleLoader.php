<?php

namespace Admissions2020\Lib;

use Admissions2020\Lib\AbstractAssetLoader;

class StyleLoader extends AbstractAssetLoader {

  public function enqueue() {
    $this->prepareAdobeFonts();
    $this->prepareGoogleFonts();
    $this->prepareNavMenuStyles();
    $this->prepareUConnBannerStyles(); 
    $this->prepareThemeStyles();

    wp_enqueue_style($this->handle);
  }

  public function adminEnqueue(string $hook) {
    if ($hook !== 'widgets.php') {
      return;
    }

    $this->prepareAdminStyles();

    wp_enqueue_style($this->adminHandle);
  }

  private function prepareThemeStyles() {
    $style_deps = ['a11y-menu', 'adobe-fonts', 'google-fonts', 'uconn-banner'];
    wp_register_style(
      $this->handle,
      $this->stylesPath,
      $style_deps
    );
  }

  private function prepareAdminStyles() {
    $style_deps = [];
    wp_register_style(
      $this->adminHandle,
      ADMISSIONS_2020_URL . '/admin/css/style.css',
      $style_deps
    );
  }

  private function prepareGoogleFonts() {
    wp_register_style(
      'google-fonts',
      'https://fonts.googleapis.com/css2?family=Archivo:ital,wght@0,400;0,600;0,700;1,400;1,600;1,700&family=DM+Serif+Display&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap',
      [],
      null
    );
  }

  private function prepareAdobeFonts() {
    wp_register_style(
      'adobe-fonts',
      'https://use.typekit.net/nmk2dgu.css',
      [],
      null
    );
  }

  private function prepareNavMenuStyles() {
    wp_register_style(
      'a11y-menu',
      ADMISSIONS_2020_URL . '/vendor/ucomm/a11y-menu/dist/main.css',
      [],
      null
    );
  }

  private function prepareUConnBannerStyles() {
    wp_register_style(
      'uconn-banner',
      ADMISSIONS_2020_URL . '/assets/css/banner.css',
      [],
      null
    );
  }
}
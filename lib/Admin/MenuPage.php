<?php

namespace Admissions2020\Lib\Admin;

use WP_Query;
use Admissions2020\Lib\BeaverBuilder\Helpers;

class MenuPage {
  public function createMenuPage() {
    add_action('admin_menu', [ $this, 'prepareMenuPage' ]);
  }

  public function prepareMenuPage() {
    add_menu_page(
      __('Admissions 2020 Theme Settings', 'admissions-2020'),
      __('Admissions 2020 Theme Settings', 'admissions-2020'),
      'manage_options',
      'admissions-2020-theme-settings',
      [ $this, 'getSettingsPage' ]
    );
  }

  public function getSettingsPage() {
    include ADMISSIONS_2020_DIR . '/includes/admin/settings-page.php';
  }

}
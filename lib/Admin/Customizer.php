<?php

namespace Admissions2020\Lib\Admin;

use WP_Customize_Manager;
use WP_Customize_Control;

class Customizer {
  public function customizeRegister() {
    add_action('customize_register', [ $this, 'handleCustomizer' ]);
  }

  public function handleCustomizer(WP_Customize_Manager $wp_customize) {
    $this->addSettings($wp_customize);
    $this->addControls($wp_customize);
  }

  /**
   * Add additional settings as options to the admissions theme
   *
   * @param WP_Customize_Manager $wp_customize
   * @return void
   */
  private function addSettings(WP_Customize_Manager $wp_customize) {
    $wp_customize->add_setting('uc-admissions-parent', [
      'default' => __('Division of Enrollment and Planning', 'admissions-2020'),
      'type' => 'option',
      'transport' => 'postMessage'
    ]);

    $wp_customize->add_setting('uc-admissions-parent-link', [
      'default' => home_url(),
      'type' => 'option',
      'transport' => 'postMessage'
    ]);
  }


  /**
   * Add customizer controls for settings
   *
   * @param WP_Customize_Manager $wp_customize
   * @return void
   */
  private function addControls(WP_Customize_Manager $wp_customize) {
    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'uc-admissions-parent-link', [
      'label' => __('Parent Site Link', 'admissions-2020'),
      'section' => 'title_tagline',
      'settings' => 'uc-admissions-parent-link'
    ]));

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'uc-admissions-parent', [
      'label' => __('Parent Site Name', 'admissions-2020'),
      'section' => 'title_tagline',
      'settings' => 'uc-admissions-parent'
    ]));
  }
}
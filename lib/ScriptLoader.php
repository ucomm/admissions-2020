<?php

namespace Admissions2020\Lib;

use Admissions2020\Lib\AbstractAssetLoader;

class ScriptLoader extends AbstractAssetLoader {

  public function enqueue() {
    $this->prepareNavMenuScript();
    $this->prepareThemeScripts();

    wp_enqueue_script($this->handle);
  }

  public function adminEnqueue(string $hook) {
    if ($hook !== 'widgets.php') return;

    $this->prepareAdminScripts();
    
    wp_enqueue_script($this->adminHandle);
  }

  private function prepareAdminScripts() {
    wp_register_script(
      $this->adminHandle,
      ADMISSIONS_2020_URL . '/admin/js/index.js',
      ['jquery'],
      false,
      true
    );
  }

  private function prepareThemeScripts() {
    $script_deps = ['a11y-menu'];
    wp_register_script(
      $this->handle,
      $this->scriptsPath,
      $script_deps,
      false,
      true
    );
  }

  private function prepareNavMenuScript() {
    wp_register_script(
      'a11y-menu',
      ADMISSIONS_2020_URL . '/vendor/ucomm/a11y-menu/dist/Navigation.min.js',
      false,
      true
    );
  }
}